# Compressive Single pixel imaging simulation and experimental results

## Name
Compressive single pixel imaging using different Hadamard orders.

## Description
Single pixel imaging (SPI) is an imaging technique which can reconstruct images using only a single photodetector. This is achieved by sampling the original image with a set of orthogonal patterns. SPI can be combined with an image reconstruction technique denominated compressive sensing in order to reduce the number of samples that must be taken for the image to be reconstructed [Vaz2020].

This dateset presents simulation and experimental results of images reconstructed using different Hadamard sampling orders. Here, the algorithm TVAL3 was used for image reconstruction.

The files names use the following configuration: TVAL3_ImageName_order_XX_SR_YY_c_ZZ

XX represents the order for the sampling: AI (Ascending Inertia), AS (Ascending Scale), TG (Total Gradient), and CC (Cake-cutting).

YY represents the sampling ratio in percentage of the maximum number of samples (image number of pixels): 1 to 20.

ZZ represents the noise level induced in the images (simulation only): 0, 0.1, and 0.5. 

Detailed information's can be found in: 

## Authors and acknowledgment
A special thanks for all the contributors of this project:
Pedro G. Vaz, Andreia Gaudêncio, João Cardoso, Luís Requicha Ferreira, Miguel Morgado. 

## Project status
The main project being this results is still under development.
